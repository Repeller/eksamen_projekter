from sense_hat import SenseHat
import time

s = SenseHat()
s.low_light = True

green = (0, 255, 0)
yellow = (255, 255, 0)
blue = (0, 0, 255)
red = (255, 0, 0)
white = (255,255,255)
nothing = (0,0,0)
pink = (255,105,180)
lightred = (255,90,90)
darkred = (155,0,0)

def Ball1():
    G = green
    Y = yellow
    B = blue
    O = nothing
    R = red
    W = white
    LR = lightred 
    DR = darkred
    logo = [
    O, O, O, O, O, O, O, O,
    O, O, R, R, R, R, O, O,
    O, R, R, R, LR, LR, R, O,
    O, DR, R, R, R, LR, R, O,
    O, DR, R, R, R, R, R, O,
    O, DR, R, R, R, R, R, O,
    O, O, R, R, R, R, O, O,
    O, O, O, O, O, O, O, O,
    ]
    return logo
    
    
def Ball2():
    G = green
    Y = yellow
    B = blue
    O = nothing
    R = red
    W = white
    LR = lightred 
    DR = darkred
    logo = [
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, R, R, R, R, O, O,
    O, DR, R, R, LR, LR, R, O,
    O, DR, R, R, R, LR, R, O,
    O, DR, R, R, R, R, R, O,
    O, DR, R, R, R, R, R, O,
    O, O, R, R, R, R, O, O,
    ]
    return logo
    

def Ball3():
    G = green
    Y = yellow
    B = blue
    O = nothing
    R = red
    W = white
    LR = lightred 
    DR = darkred
    logo = [
    O, O, R, R, R, R, O, O,
    O, DR, R, R, LR, LR, R, O,
    O, DR, R, R, R, LR, R, O,
    O, DR, R, R, R, R, R, O,
    O, DR, R, R, R, R, R, O,
    O, O, R, R, R, R, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    ]
    return logo



images = [Ball1, Ball2, Ball3]
count = 0

while True: 
    s.set_pixels(images[count % len(images)]())
    time.sleep(.75)
    count += 1
