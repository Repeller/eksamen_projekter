from sense_hat import SenseHat
import time

s = SenseHat()
s.low_light = True

green = (0, 255, 0)
yellow = (255, 255, 0)
blue = (0, 0, 255)
red = (255, 0, 0)
white = (255,255,255)
nothing = (0,0,0)
pink = (255,105, 180)

def Bone1():
    G = green
    Y = yellow
    B = blue
    O = nothing
    R = red
    W = white
    logo = [
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, W, O, O,
    O, O, O, O, O, W, W, O,
    O, O, O, O, W, O, O, O,
    O, O, O, W, O, O, O, O,
    O, W, W, O, O, O, O, O,
    O, O, W, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    ]
    return logo
    

    
def Bone2():
    G = green
    Y = yellow
    B = blue
    O = nothing
    R = red
    W = white
    logo = [
    O, O, O, O, O, O, O, O,
    O, O, W, O, O, O, O, O,
    O, W, W, O, O, O, O, O,
    O, O, O, W, O, O, O, O,
    O, O, O, O, W, O, O, O,
    O, O, O, O, O, W, W, O,
    O, O, O, O, O, W, O, O,
    O, O, O, O, O, O, O, O,
    ]
    return logo


images = [Bone1, Bone2]
count = 0

while True: 
    s.set_pixels(images[count % len(images)]())
    time.sleep(.75)
    count += 1
