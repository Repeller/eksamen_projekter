from sense_hat import SenseHat
import time

s = SenseHat()
s.low_light = True

green = (0, 255, 0)
yellow = (255, 255, 0)
blue = (0, 0, 255)
red = (255, 0, 0)
white = (255,255,255)
nothing = (0,0,0)
pink = (255,105,180)
lightred = (255,90,90)
darkred = (155,0,0)

def Ball():
    G = green
    Y = yellow
    B = blue
    O = nothing
    R = red
    W = white
    LR = lightred 
    DR = darkred
    logo = [
    W, W, R, R, R, R, W, W,
    W, R, R, LR, LR, LR, R, W,
    DR, R, R, R, LR, LR, R, R,
    DR, R, R, R, R, LR, R, R,
    DR, R, R, R, R, R, R, R,
    DR, R, R, R, R, R, R, R,
    G, DR, R, R, R, R, R, G,
    G, G, DR, R, R, R, G, G,
    ]
    return logo
    



images = [Ball]
count = 0

while True: 
    s.set_pixels(images[count % len(images)]())
    time.sleep(.75)
    count += 1
