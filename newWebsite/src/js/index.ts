import axios, {
    AxiosResponse,
    AxiosError
} from "../../node_modules/axios/index"; // Don't worry about red lines here ...

// object structure as interfaces
// interface IUser {
//     id: number;
//     username: number;
//     password: string;
// }
interface IStatus {
    id: number;
    mood: number;
    food: number;
    hp: number;
    fkUserId: number;
    when: string;
}
interface IPlay {
    id: number;
    text: string;
    when: Date;
    fkUserId: number;
}
interface IPower {
    id: number;
    text: string;
    when: Date;
    fkUserId: number;
}
interface IFood {
    id: number;
    text: string;
    when: Date;
    fkUserId: number;
}

// html reference variables
//let User: HTMLDivElement = <HTMLDivElement>document.getElementById("contentUser");
let Food: HTMLDivElement = <HTMLDivElement>document.getElementById("contentFoodEvents");
let Power: HTMLDivElement = <HTMLDivElement>document.getElementById("contentPowerEvents");
let Status: HTMLDivElement = <HTMLDivElement>document.getElementById("contentStatusEvents");
let Play: HTMLDivElement = <HTMLDivElement>document.getElementById("contentPlayEvents");

function test(): void {

}

//methods that always runs
// axios.get<IUser[]>("https://tami.azurewebsites.net/api/user")
//     .then((response: AxiosResponse<IUser[]>) => {
//         let data: IUser[] = response.data;
//         User.innerHTML = JSON.stringify(data);
//         data.forEach(element => {
//             console.log(User.title);
//         });
//     })
//     .catch((error: AxiosError) => {
//         console.log(error);
//         User.innerHTML = error.message;
//     });

axios.get<IFood[]>("https://tami.azurewebsites.net/api/EventFood")
    .then((response: AxiosResponse<IFood[]>) => {
        let data: IFood[] = response.data;
        let tableFood: HTMLTableElement = <HTMLTableElement>document.getElementById("tableFoodBody");
        let index: number = 0;
        //Food.innerHTML = JSON.stringify(data);

        data.forEach(element => {
            console.log(Food.title);
            let row = tableFood.insertRow(index);

            //console.log("text: ", element.Text.toString());
            //console.log("user id:", element.FkUserId.toString());

            console.log("id: ", element.id.toString());
            row.insertCell(0).innerHTML = index.toString();
            row.insertCell(-1).innerHTML = element.id.toString(); 
            row.insertCell(-1).innerHTML = element.text.toString();
            row.insertCell(-1).innerHTML = (element.when.toString());
            row.insertCell(-1).innerHTML = element.fkUserId.toString();
            index++;
        });
    })
    .catch((error: AxiosError) => {
        console.log(error);
        Food.innerHTML = error.message;
    });

axios.get<IPlay[]>("https://tami.azurewebsites.net/api/EventPlay")
    .then((response: AxiosResponse<IPlay[]>) => {
        let data: IPlay[] = response.data;
       
        let tablePlay: HTMLTableElement = <HTMLTableElement>document.getElementById("tablePlayBody");
        let index: number = 0;

        data.forEach(element => {
            //console.log(Play.title);
            let row = tablePlay.insertRow(index);

            //console.log("text: ", element.Text.toString());
            //console.log("user id:", element.FkUserId.toString());

            console.log("id: ", element.id.toString());
            row.insertCell(0).innerHTML = index.toString();
            row.insertCell(-1).innerHTML = element.id.toString(); 
            row.insertCell(-1).innerHTML = element.text.toString();
            row.insertCell(-1).innerHTML = (element.when.toString());
            row.insertCell(-1).innerHTML = element.fkUserId.toString();
            index++;
        });
    })
    .catch((error: AxiosError) => {
        console.log(error);
        Play.innerHTML = error.message;
    });

axios.get<IPower[]>("https://tami.azurewebsites.net/api/EventPower")
    .then((response: AxiosResponse<IPower[]>) => {
        let data: IPower[] = response.data;
        
        let tablePower: HTMLTableElement = <HTMLTableElement>document.getElementById("tablePowerBody");
        let index: number = 0;

        data.forEach(element => {
            //console.log(Play.title);
            let row = tablePower.insertRow(index);

            //console.log("text: ", element.Text.toString());
            //console.log("user id:", element.FkUserId.toString());

            console.log("id: ", element.id.toString());
            row.insertCell(0).innerHTML = index.toString();
            row.insertCell(-1).innerHTML = element.id.toString(); 
            row.insertCell(-1).innerHTML = element.text.toString();
            row.insertCell(-1).innerHTML = (element.when.toString());
            row.insertCell(-1).innerHTML = element.fkUserId.toString();
            index++;
        });
    })
    .catch((error: AxiosError) => {
        console.log(error);
        Power.innerHTML = error.message;
    });

axios.get<IStatus[]>("https://tami.azurewebsites.net/api/Status")
    .then((response: AxiosResponse<IStatus[]>) => {
        let data: IStatus[] = response.data;
        let tableStatus: HTMLTableElement = <HTMLTableElement>document.getElementById("tableStatusBody");
        let index: number = 0;

        data.forEach(element => {
            //console.log(Play.title);
            let row = tableStatus.insertRow(index);

            //console.log("text: ", element.Text.toString());
            //console.log("user id:", element.FkUserId.toString());

            console.log("id: ", element.id.toString());
            row.insertCell(0).innerHTML = index.toString();
            row.insertCell(-1).innerHTML = element.id.toString(); 
            row.insertCell(-1).innerHTML = element.mood.toString();
            row.insertCell(-1).innerHTML = (element.food.toString());
            row.insertCell(-1).innerHTML = element.hp.toString();
            row.insertCell(-1).innerHTML = element.when.toString();
            row.insertCell(-1).innerHTML = element.fkUserId.toString();

            index++;
        });
    })
    .catch((error: AxiosError) => {
        console.log(error);
        Status.innerHTML = error.message;
    });