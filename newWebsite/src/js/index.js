"use strict";
exports.__esModule = true;
var index_1 = require("../../node_modules/axios/index"); // Don't worry about red lines here ...
// html reference variables
var User = document.getElementById("contentUser");
var Food = document.getElementById("contentFoodEvents");
var Power = document.getElementById("contentPowerEvents");
var Status = document.getElementById("contentStatusEvents");
var Play = document.getElementById("contentPlayEvents");
function test() {
}
// methods that always runs
index_1["default"].get("https://tami.azurewebsites.net/api/user")
    .then(function (response) {
    var data = response.data;
    User.innerHTML = JSON.stringify(data);
    data.forEach(function (element) {
        console.log(User.title);
    });
})["catch"](function (error) {
    console.log(error);
    User.innerHTML = error.message;
});
index_1["default"].get("https://tami.azurewebsites.net/api/EventFood")
    .then(function (response) {
    var data = response.data;
    var tableFood = document.getElementById("tableFoodBody");
    var index = 0;
    //Food.innerHTML = JSON.stringify(data);
    data.forEach(function (element) {
        console.log(Food.title);
        var row = tableFood.insertRow(index);
        row.insertCell(0).innerHTML = element.id.toString();
        row.insertCell(-1).innerHTML = element.Text.toString();
        row.insertCell(-1).innerHTML = element.When.toString();
        row.insertCell(-1).innerHTML = element.FkUserId.toString();
        index++;
    });
})["catch"](function (error) {
    console.log(error);
    Food.innerHTML = error.message;
});
index_1["default"].get("https://tami.azurewebsites.net/api/EventPlay")
    .then(function (response) {
    var data = response.data;
    Play.innerHTML = JSON.stringify(data);
    data.forEach(function (element) {
        console.log(Play.title);
    });
})["catch"](function (error) {
    console.log(error);
    Play.innerHTML = error.message;
});
index_1["default"].get("https://tami.azurewebsites.net/api/EventPower")
    .then(function (response) {
    var data = response.data;
    Power.innerHTML = JSON.stringify(data);
    data.forEach(function (element) {
        console.log(Power.title);
    });
})["catch"](function (error) {
    console.log(error);
    Power.innerHTML = error.message;
});
index_1["default"].get("https://tami.azurewebsites.net/api/Status")
    .then(function (response) {
    var data = response.data;
    Status.innerHTML = JSON.stringify(data);
    data.forEach(function (element) {
        console.log(Status.title);
    });
})["catch"](function (error) {
    console.log(error);
    Status.innerHTML = error.message;
});
