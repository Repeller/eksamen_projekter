﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

using WebRestApi.Models;
using WebRestApi.Controllers;


namespace WebRestApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();

            //GetSensor();

        }

        public static async void GetSensor()
        {

            //Creates a UdpClient for reading incoming data.
            UdpClient udpReceiver = new UdpClient(11678);

            // This IPEndPoint will allow you to read datagrams sent from any ip-source on port 7000

            IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, 11678);

            // receivingUdpClient.Connect(RemoteIpEndPoint); what is this used for ??

            // Blocks until a message arrives on this socket from a remote host.
            Console.WriteLine("Receiver is blocked");

            try
            {
                while (true)
                {
                    Byte[] receiveBytes = udpReceiver.Receive(ref RemoteIpEndPoint);

                    string receivedData = Encoding.ASCII.GetString(receiveBytes);
                    if (receivedData.Equals("STOP.Secret")) throw new Exception("Receiver stopped");

                    Console.WriteLine("Sender: " + receivedData.ToString());

                    // string[] textLines = receivedData.Split(' '); is possible but a little more difficult 
                    //best to split by '\n' or \r\ and then split by ' ' or  ':'
                    string[] textLines = receivedData.Split('\n');
                    Console.WriteLine();
                    // write to console
                    for (int index = 0; index < textLines.Length; index++)
                        Console.WriteLine(textLines[index]);
                    Console.WriteLine();

                    // used for testing format 
                    //foreach (string s in textLines)
                    //{
                    //    Console.WriteLine(s);
                    //}
                    // Console.ReadLine();

                    // check if it is status or event
                    if (textLines[0].Contains('s'))
                    {
                        int mood = Convert.ToInt32(textLines[1]);
                        int food = Convert.ToInt32(textLines[2]);
                        int health = Convert.ToInt32(textLines[3]);
                        int userFk = Convert.ToInt32(textLines[4]);

                        Status tempStatus = new Status(0, mood, food, health, DateTime.Now, userFk);

                        StatusController statusController = new StatusController();
                        statusController.Post(tempStatus);
                    }
                    // power event
                    else if (textLines[0].Contains('p'))
                    {
                        string text = textLines[1];
                        int userFk = Convert.ToInt32(textLines[2]);

                        EventPower power = new EventPower(0, text, DateTime.Now, userFk);
                        EventPowerController powerController = new EventPowerController();
                        powerController.Post(power);
                    }
                    // play event
                    else if (textLines[0].Contains('y'))
                    {
                        string text = textLines[1];
                        int userFk = Convert.ToInt32(textLines[2]);

                        EventPlay play = new EventPlay(0, text, DateTime.Now, userFk);
                        EventPlayController playController = new EventPlayController();
                        playController.Post(play);
                    }
                    // food event
                    else if (textLines[0].Contains('f'))
                    {
                        string text = textLines[1];
                        int userFk = Convert.ToInt32(textLines[2]);

                        EventFood food = new EventFood(0, text, DateTime.Now, userFk);
                        EventFoodController foodController = new EventFoodController();
                        foodController.Post(food);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
