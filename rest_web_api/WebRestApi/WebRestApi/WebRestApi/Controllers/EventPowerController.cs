﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebRestApi.DbBuilds;
using WebRestApi.Models;

namespace WebRestApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EventPowerController : ControllerBase
    {
        public ManageEventPower ManageEvents = new ManageEventPower();

        public EventPowerController()
        {
            ManageEvents.PowerEvents = ManageEvents.LoadPowerEvents();
        }

        /// <summary>
        /// gets all the EventPowers
        /// </summary>
        /// <returns>the list of all EventPowers</returns>
        [HttpGet]
        public IEnumerable<EventPower> Get()
        {
            return ManageEvents.PowerEvents;
        }

        // GET: api/Powers/5
        //[HttpGet("{id}", Name = "Get")]
        /// <summary>
        /// get one EventPower by id
        /// </summary>
        /// <param name="id">the id of the EventPower you want</param>
        /// <returns>the EventPower you are looking for</returns>
        [HttpGet]
        [Route("{id}")]
        public EventPower Get(int id)
        {
            //Item error = new Item(0, "error", "error", 404.404);

            return ManageEvents.Get(id);
        }

        // POST: api/Powers
        /// <summary>
        /// post one EventPower to the list
        /// </summary>
        /// <param name="value">the EventPower you want to add to the list</param>
        [HttpPost]
        public void Post([FromBody] EventPower value)
        {
            ManageEvents.Post(value);
        }

        // PUT: api/Powers/5
        //[HttpPut("{id}")]
        /// <summary>
        /// put/edit one EventPower's values and replace them with new ones
        /// </summary>
        /// <param name="id">the id of the EventPower to put into</param>
        /// <param name="value">the value that will be put into the EventPower</param>
        [HttpPut]
        [Route("{id}")]
        public void Put(int id, [FromBody] EventPower value)
        {
            ManageEvents.Put(id, value);
        }

        // DELETE: api/ApiWithActions/5
        /// <summary>
        /// delete one EventPower from the list
        /// </summary>
        /// <param name="id">the id of the EventPower you want to delete</param>
        [HttpDelete]
        [Route("{id}")]
        public void Delete(int id)
        {
            ManageEvents.Delete(id);
        }

        #region Search funktions (not in use)
        //// new custom method
        ///// <summary>
        ///// Get all EventFood that have a specific text/name
        ///// </summary>
        ///// <param name="substring">the name you want to look for</param>
        ///// <returns>the EventFoods that was found, that had the name you are looking for</returns>
        //[HttpGet]
        //[Route("Name/{substring}")]
        //public IEnumerable<Item> GetFromSubstring(string substring)
        //{
        //    List<Item> tempList = manageItems.Items.ToList();
        //    return tempList.FindAll(i => i.Name.Contains(substring));
        //}

        //// new custom method
        ///// <summary>
        ///// get all the EventFoods that have a 'low' quality
        ///// </summary>
        ///// <returns>the list of found 'low' EventFoods</returns>
        //[HttpGet]
        //[Route("Low/")]
        //public IEnumerable<Item> GetAllLow()
        //{
        //    List<Item> tempList = manageItems.Items.ToList();
        //    return tempList.FindAll(i => i.Quality.Contains("low"));
        //}

        //// new custom method
        ///// <summary>
        ///// get all the items that have a 'middle' quality
        ///// </summary>
        ///// <returns>the list of found 'middle' items</returns>
        //[HttpGet]
        //[Route("Middle/")]
        //public IEnumerable<Item> GetAllMiddle()
        //{
        //    List<Item> tempList = manageItems.Items.ToList();
        //    return tempList.FindAll(i => i.Quality.Contains("middle"));
        //}

        //// new custom method
        ///// <summary>
        ///// get all the items that have a 'high' quality
        ///// </summary>
        ///// <returns>the list of found 'high' items</returns>
        //[HttpGet]
        //[Route("High/")]
        //public IEnumerable<Item> GetAllHigh()
        //{
        //    List<Item> tempList = manageItems.Items.ToList();
        //    return tempList.FindAll(i => i.Quality.Contains("high"));
        //}

        ///// <summary>
        ///// Search for items that are in between 2 quantity values
        ///// </summary>
        ///// <param name="filter">custom class, that have 2 string props "LowQuantity" and "HighQuantity"</param>
        ///// <returns>a list of all the found items, that was in between the 2 quantity values</returns>
        //[HttpGet]
        //[Route("Search")]
        //public IEnumerable<Item> GetWithFilter([FromQuery] Models.FilterItem filter)
        //{
        //    bool isLow = !String.IsNullOrEmpty(filter.LowQuantity);
        //    bool isHigh = !String.IsNullOrEmpty(filter.HighQuantity);

        //    double lowValue = Convert.ToDouble(filter.LowQuantity);
        //    double highValue = Convert.ToDouble(filter.HighQuantity);

        //    List<Item> tempList = manageItems.Items.ToList();

        //    // switch between the values, in case of error 40
        //    if (lowValue > highValue)
        //    {
        //        double temp = lowValue;
        //        lowValue = highValue;
        //        highValue = temp;
        //    }

        //    if (isLow && isHigh == false) // only low
        //    {
        //        return tempList.FindAll(i => i.Quantity == lowValue);
        //    }
        //    else if (isHigh && isLow == false) // only high
        //    {
        //        return tempList.FindAll(i => (i.Quantity >= 0) && (i.Quantity <= highValue));
        //    }
        //    else if (isHigh && isLow) // both low and high
        //    {
        //        return tempList.FindAll(i => (i.Quantity >= lowValue) && (i.Quantity <= highValue));
        //    }

        //    return new List<Item>();
        //}

        #endregion
        #region old
        //// GET: api/EventFood
        //[HttpGet]
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        //// GET: api/EventFood/5
        //[HttpGet("{id}", Name = "Get")]
        //public string Get(int id)
        //{
        //    return "value";
        //}

        //// POST: api/EventFood
        //[HttpPost]
        //public void Post([FromBody] string value)
        //{
        //}

        //// PUT: api/EventFood/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody] string value)
        //{
        //}

        //// DELETE: api/ApiWithActions/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //} 
        #endregion
    }
}
