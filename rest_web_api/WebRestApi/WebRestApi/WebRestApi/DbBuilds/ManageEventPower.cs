﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using WebRestApi.Models;

namespace WebRestApi.DbBuilds
{
    public class ManageEventPower
    {
        // fields
        private const string _connectionString = "Server=tcp:zealand-eksamen-opgaver-server.database.windows.net,1433;" +
                                                 "Initial Catalog=ZealandEksamenOpgaverDB;Persist Security Info=False;" +
                                                 "User ID=ZealandAdmin;" +
                                                 "Password=!xMrIATNC0Lg%6o8oj&b^Aojc6uSjzNwA$t9F;" +
                                                 "MultipleActiveResultSets=False;" +
                                                 "Encrypt=True;" +
                                                 "TrustServerCertificate=False;" +
                                                 "Connection Timeout=30;";

        private const string GET_ALL = "select * from Tam.EventPower";
        private const string GET_ONE = "select * from Tam.EventPower WHERE Id = @ID";
        private const string POST_ONE = "insert into Tam.EventPower (Text, FkUserId) VALUES (@TEXT, @FKUSERID)";

        private const string PUT_ONE =
            "UPDATE Tam.EventPower SET Text = @TEXT, FkUserId = @FKUSERID WHERE Id = @ID";

        private const string DELETE_ONE = "DELETE FROM Tam.EventPower WHERE Id = @ID";

        // prop
        public IEnumerable<EventPower> PowerEvents { get; set; }


        public IEnumerable<EventPower> Get()
        {
            return PowerEvents;
        }

        /// <summary>
        /// reads the data from the reader and returns a eventPower obj
        /// </summary>
        /// <param name="reader">the reader in use</param>
        /// <returns>the eventPower obj from the reader</returns>
        protected EventPower ReadNextElement(SqlDataReader reader)
        {
            EventPower eventPower = new EventPower();

            eventPower.Id = reader.GetInt32(0);
            eventPower.Text = reader.GetString(1);
            eventPower.When = reader.GetDateTime(2);
            eventPower.FkUserId = reader.GetInt32(3);

            return eventPower;
        }

        /// <summary>
        /// used to load everything into the list
        /// </summary>
        /// <returns>the list of all eventPower</returns>
        public IEnumerable<EventPower> LoadPowerEvents()
        {
            List<EventPower> tempPowers = new List<EventPower>();
            using (SqlConnection Conn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(GET_ALL, Conn))
            {
                Conn.Open();

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    EventPower value = ReadNextElement(reader);
                    tempPowers.Add(value);
                }
                reader.Close();
                Conn.Close();
            }

            return tempPowers;
        }

        public EventPower Get(int id)
        {
            EventPower tempPower = new EventPower();

            using (SqlConnection Conn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(GET_ONE, Conn))
            {
                cmd.Parameters.AddWithValue("@ID", id);
                Conn.Open();

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    EventPower value = ReadNextElement(reader);
                    if (value.Id == id)
                    {
                        tempPower = value;
                        break;
                    }
                }
                reader.Close();
                Conn.Close();
            }

            return tempPower;
        }

        public void Post(EventPower value)
        {
            EventPower tempPower = new EventPower();

            using (SqlConnection Conn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(POST_ONE, Conn))
            {
                // we will not use the id, since that get created in the DB
                cmd.Parameters.AddWithValue("@TEXT", value.Text);
                cmd.Parameters.AddWithValue("@FKUSERID", value.FkUserId);

                Conn.Open();

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    tempPower = ReadNextElement(reader);
                }
                reader.Close();
                Conn.Close();

                // TODO : maybe remove this line
                PowerEvents = LoadPowerEvents();
            }
        }

        public void Put(int id, EventPower value)
        {
            EventPower tempPower = new EventPower();

            using (SqlConnection Conn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(PUT_ONE, Conn))
            {
                // we will not use the id, since that get created in the DB
                cmd.Parameters.AddWithValue("@ID", id);
                cmd.Parameters.AddWithValue("@TEXT", value.Text);
                cmd.Parameters.AddWithValue("@FKUSERID", value.FkUserId);

                Conn.Open();

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    tempPower = ReadNextElement(reader);
                }
                reader.Close();
                Conn.Close();

                // TODO : maybe remove this line
                PowerEvents = LoadPowerEvents();
            }
        }

        public void Delete(int id)
        {
            EventPower tempPower = new EventPower();

            using (SqlConnection Conn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(DELETE_ONE, Conn))
            {
                // we will not use the id, since that get created in the DB
                cmd.Parameters.AddWithValue("@ID", id);

                Conn.Open();

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    tempPower = ReadNextElement(reader);
                }
                reader.Close();
                Conn.Close();

                // TODO : maybe remove this line
                PowerEvents = LoadPowerEvents();
            }
        }
    }
}
