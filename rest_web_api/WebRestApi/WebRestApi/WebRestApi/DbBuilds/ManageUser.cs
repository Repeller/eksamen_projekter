﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using WebRestApi.Models;

namespace WebRestApi.DbBuilds
{
    public class ManageUser
    {
        // fields
        private const string _connectionString = "Server=tcp:zealand-eksamen-opgaver-server.database.windows.net,1433;" +
                                                 "Initial Catalog=ZealandEksamenOpgaverDB;Persist Security Info=False;" +
                                                 "User ID=ZealandAdmin;" +
                                                 "Password=!xMrIATNC0Lg%6o8oj&b^Aojc6uSjzNwA$t9F;" +
                                                 "MultipleActiveResultSets=False;" +
                                                 "Encrypt=True;" +
                                                 "TrustServerCertificate=False;" +
                                                 "Connection Timeout=30;";

        private const string GET_ALL = "select * FROM Tam.Account";
        private const string GET_ONE = "select * from Tam.Account WHERE Id = @ID";
        private const string POST_ONE = "insert into Tam.Account (Username, Password) VALUES (@USERNAME, @PASSWORD)";

        private const string PUT_ONE =
            "UPDATE Tam.Account SET Username = @USERNAME, Password = @PASSWORD WHERE Id = @ID";

        private const string DELETE_ONE = "DELETE FROM Tam.Account WHERE Id = @ID";

        // prop
        public IEnumerable<User> Users { get; set; }


        public IEnumerable<User> Get()
        {
            return Users;
        }

        /// <summary>
        /// reads the data from the reader and returns a user obj
        /// </summary>
        /// <param name="reader">the reader in use</param>
        /// <returns>the user obj from the reader</returns>
        protected User ReadNextElement(SqlDataReader reader)
        {
            User user = new User();

            user.Id = reader.GetInt32(0);
            user.Username = reader.GetString(1);
            user.Password = reader.GetString(2);

            return user;
        }

        /// <summary>
        /// used to load everything into the list
        /// </summary>
        /// <returns>the list of all users</returns>
        public IEnumerable<User> LoadUsers()
        {
            List<User> tempUsers = new List<User>();
            using (SqlConnection Conn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(GET_ALL, Conn))
            {
                Conn.Open();

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    User value = ReadNextElement(reader);
                    tempUsers.Add(value);
                }
                reader.Close();
                Conn.Close();
            }

            return tempUsers;
        }

        public User Get(int id)
        {
            User tempUser = new User();

            using (SqlConnection Conn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(GET_ONE, Conn))
            {
                cmd.Parameters.AddWithValue("@ID", id);
                Conn.Open();

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    User value = ReadNextElement(reader);
                    if (value.Id == id)
                    {
                        tempUser = value;
                        break;
                    }
                }
                reader.Close();
                Conn.Close();
            }

            return tempUser;
        }

        public void Post(User value)
        {
            User tempUser = new User();

            using (SqlConnection Conn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(POST_ONE, Conn))
            {
                // we will not use the id, since that get created in the DB
                cmd.Parameters.AddWithValue("@USERNAME", value.Username);
                cmd.Parameters.AddWithValue("@PASSWORD", value.Password);

                Conn.Open();

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    tempUser = ReadNextElement(reader);
                }
                reader.Close();
                Conn.Close();

                // TODO : maybe remove this line
                Users = LoadUsers();
            }
        }

        public void Put(int id, User value)
        {
            User tempUser = new User();

            using (SqlConnection Conn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(PUT_ONE, Conn))
            {
                // we will not use the id, since that get created in the DB
                cmd.Parameters.AddWithValue("@ID", id);
                cmd.Parameters.AddWithValue("@USERNAME", value.Username);
                cmd.Parameters.AddWithValue("@PASSWORD", value.Password);

                Conn.Open();

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    tempUser = ReadNextElement(reader);
                }
                reader.Close();
                Conn.Close();

                // TODO : maybe remove this line
                Users = LoadUsers();
            }
        }

        public void Delete(int id)
        {
            User tempUser = new User();

            using (SqlConnection Conn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(DELETE_ONE, Conn))
            {
                // we will not use the id, since that get created in the DB
                cmd.Parameters.AddWithValue("@ID", id);

                Conn.Open();

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    tempUser = ReadNextElement(reader);
                }
                reader.Close();
                Conn.Close();

                // TODO : maybe remove this line
                Users = LoadUsers();
            }
        }
    }
}
