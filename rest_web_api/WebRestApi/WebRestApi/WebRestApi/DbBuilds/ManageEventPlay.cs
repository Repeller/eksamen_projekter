﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using WebRestApi.Models;

namespace WebRestApi.DbBuilds
{
    public class ManageEventPlay
    {
        // fields
        private const string _connectionString = "Server=tcp:zealand-eksamen-opgaver-server.database.windows.net,1433;" +
                                                 "Initial Catalog=ZealandEksamenOpgaverDB;Persist Security Info=False;" +
                                                 "User ID=ZealandAdmin;" +
                                                 "Password=!xMrIATNC0Lg%6o8oj&b^Aojc6uSjzNwA$t9F;" +
                                                 "MultipleActiveResultSets=False;" +
                                                 "Encrypt=True;" +
                                                 "TrustServerCertificate=False;" +
                                                 "Connection Timeout=30;";

        private const string GET_ALL = "select * from Tam.EventPlay";
        private const string GET_ONE = "select * from Tam.EventPlay WHERE Id = @ID";
        private const string POST_ONE = "insert into Tam.EventPlay (Text, FkUserId) VALUES (@TEXT, @FKUSERID)";

        private const string PUT_ONE =
            "UPDATE Tam.EventPlay SET Text = @TEXT, FkUserId = @FKUSERID WHERE Id = @ID";

        private const string DELETE_ONE = "DELETE FROM Tam.EventPlay WHERE Id = @ID";

        // prop
        public IEnumerable<EventPlay> PlayEvents { get; set; }


        public IEnumerable<EventPlay> Get()
        {
            return PlayEvents;
        }

        /// <summary>
        /// reads the data from the reader and returns a EventPlay obj
        /// </summary>
        /// <param name="reader">the reader in use</param>
        /// <returns>the EventPlay obj from the reader</returns>
        protected EventPlay ReadNextElement(SqlDataReader reader)
        {
            EventPlay play = new EventPlay();

            play.Id = reader.GetInt32(0);
            play.Text = reader.GetString(1);
            play.When = reader.GetDateTime(2);
            play.FkUserId = reader.GetInt32(3);

            return play;
        }

        /// <summary>
        /// used to load everything into the list
        /// </summary>
        /// <returns>the list of all EventPlay</returns>
        public IEnumerable<EventPlay> LoadPlayEvents()
        {
            List<EventPlay> tempPlays = new List<EventPlay>();
            using (SqlConnection Conn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(GET_ALL, Conn))
            {
                Conn.Open();

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    EventPlay value = ReadNextElement(reader);
                    tempPlays.Add(value);
                }
                reader.Close();
                Conn.Close();
            }

            return tempPlays;
        }

        public EventPlay Get(int id)
        {
            EventPlay tempPlay = new EventPlay();

            using (SqlConnection Conn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(GET_ONE, Conn))
            {
                cmd.Parameters.AddWithValue("@ID", id);
                Conn.Open();

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    EventPlay value = ReadNextElement(reader);
                    if (value.Id == id)
                    {
                        tempPlay = value;
                        break;
                    }
                }
                reader.Close();
                Conn.Close();
            }

            return tempPlay;
        }

        public void Post(EventPlay value)
        {
            EventPlay tempPlay = new EventPlay();

            using (SqlConnection Conn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(POST_ONE, Conn))
            {
                // we will not use the id, since that get created in the DB
                cmd.Parameters.AddWithValue("@TEXT", value.Text);
                cmd.Parameters.AddWithValue("@FKUSERID", value.FkUserId);

                Conn.Open();

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    tempPlay = ReadNextElement(reader);
                }
                reader.Close();
                Conn.Close();

                // TODO : maybe remove this line
                PlayEvents = LoadPlayEvents();
            }
        }

        public void Put(int id, EventPlay value)
        {
            EventPlay tempPlay = new EventPlay();

            using (SqlConnection Conn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(PUT_ONE, Conn))
            {
                // we will not use the id, since that get created in the DB
                cmd.Parameters.AddWithValue("@ID", id);
                cmd.Parameters.AddWithValue("@TEXT", value.Text);
                cmd.Parameters.AddWithValue("@FKUSERID", value.FkUserId);

                Conn.Open();

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    tempPlay = ReadNextElement(reader);
                }
                reader.Close();
                Conn.Close();

                // TODO : maybe remove this line
                PlayEvents = LoadPlayEvents();
            }
        }

        public void Delete(int id)
        {
            EventPlay tempPlay = new EventPlay();

            using (SqlConnection Conn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(DELETE_ONE, Conn))
            {
                // we will not use the id, since that get created in the DB
                cmd.Parameters.AddWithValue("@ID", id);

                Conn.Open();

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    tempPlay = ReadNextElement(reader);
                }
                reader.Close();
                Conn.Close();

                // TODO : maybe remove this line
                PlayEvents = LoadPlayEvents();
            }
        }
    }
}
