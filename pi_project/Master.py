from sense_hat import SenseHat
import time
import random
from done.Animations import *
s = SenseHat()
s.clear()
s.low_light = True

count = 0

images2 = mad_pixels()
# yellow face
images3 = hunger1_pixels()
# white face
images4 = hunger2_pixels()
images5 = bone_pixels()
images6 = ball_pixels()
images7 = happy2_pixels()
# new version
images8 = happy_pixels()
images9 = bouncingBall()

while True:
    countOfPhotos = len(images5)
    if count != countOfPhotos:
        s.set_pixels(images5[count])

    elif count == countOfPhotos:
        count = -1

    time.sleep(.3)
    count += 1
