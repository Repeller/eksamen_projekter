from sense_hat import SenseHat
import time
import random

s = SenseHat()
s.clear()
s.low_light = True

g = (0, 255, 0)
b = (0, 0, 255)
n = (0,0,0)
r = (255, 0, 0)

#listOfColors = [green, blue, nothing]

# the count of turns
count = 0

# the values needed to see when it have been turned
turnedLeft = False
turnedMid = False
turnedRight = False

leftP = [b, b, b, b, b, b, b, b,
        b, b, b, b, b, b, b, b,
        b, b, b, b, b, b, b, b,
        b, b, b, b, b, b, b, b,
        b, b, b, b, b, b, b, b,
        b, b, b, b, b, b, b, b,
        b, b, b, b, b, b, b, b,
        b, b, b, b, b, b, b, b]
        
midP = [g, g, g, g, g, g, g, g,
        n, n, n, n, n, n, n, n,
        g, g, g, g, g, g, g, g,
        n, n, n, n, n, n, n, n,
        g, g, g, g, g, g, g, g,
        n, n, n, n, n, n, n, n,
        g, g, g, g, g, g, g, g,
        n, n, n, n, n, n, n, n]
        
rightP = [r, r, r, r, r, r, r, r,
          r, r, r, r, r, r, r, r,
          r, r, r, r, r, r, r, r,
          r, r, r, r, r, r, r, r,
          r, r, r, r, r, r, r, r,
          r, r, r, r, r, r, r, r,
          r, r, r, r, r, r, r, r,
          r, r, r, r, r, r, r, r]

emptyP = [n, n, n, n, n, n, n, n,
          n, n, n, n, n, n, n, n,
          n, n, n, n, n, n, n, n,
          n, n, n, n, n, n, n, n,
          n, n, n, n, n, n, n, n,
          n, n, n, n, n, n, n, n,
          n, n, n, n, n, n, n, n,
          n, n, n, n, n, n, n, n]

images = []

while True: 
  o = s.get_orientation()
  pitch = o["pitch"]
  # roll = o["roll"]
  # yaw = o["yaw"]
  
  if count < 10:
    if float(pitch) >= 50.0 and float(pitch) <= 80.0:
      # make left
      turnedLeft = True
      turnedMid = False
      count +=1
      s.set_pixels(leftP)
    
    if float(pitch) <=20 and -float(pitch) >= -350:
      # back to mid
      turnedMid = True
      turnedLeft = False
      turnedRight = False
      s.set_pixels(midP)
    
    if float(pitch) >= 250 and float(pitch) <= 280:
      # make right
      turnedRight = True
      turnedMid = False
      count +=1
      s.set_pixels(rightP)
    
  print("turned left: " + str(turnedLeft))
  print("turned mid: " + str(turnedMid))
  print("turned right: " + str(turnedRight))
  print("pitch: " + str(pitch))
  time.sleep(.3)