from sense_hat import SenseHat
import time
import random

s = SenseHat()
s.low_light = True

green = (0, 255, 0)
yellow = (255, 255, 0)
blue = (0, 0, 255)
red = (255, 0, 0)
white = (255,255,255)
nothing = (0,0,0)
pink = (255,105, 180)

listOfColors = [green, yellow, blue, red, white, nothing, pink]

count = 0

while True: 
    textValue = s.get_orientation_degrees()
    #s.set_pixels(count, len(images)() )
    s.show_message(str(textValue), 0.10, blue)
    #s.set_pixels()
    time.sleep(.75)
    count += 1