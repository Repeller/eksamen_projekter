import json
# writen by Ziegler 10.12.19

# here we open a file and call it "json_file"
# see that we don't give "w" as the second parameter
# this is because we don't write to it, we are just reading
with open('data.txt') as json_file:
    data = json.load(json_file)
    for s in data['status']:
        print('mood: ' + str(s['mood']))
        print('food: ' + str(s['food']))
        print('hp: ' + str(s['hp']))
        print('- . - . - . - . -')
# we are looping through all the data in the array
# we are seting the data handler up, so it can understand the json data
# since I stored the data as an int, I need to convert it to string
# by using str( 69 ) to convert anything to string _ "69"
