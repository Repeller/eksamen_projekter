import saveHandler
import json
import socket
import sys

# Create a UDP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

server_address = ('https://localhost:44326/', 11678)
message = b'This is the message.  It will be repeated.'

text = "test"
fk = 1
Ttype = 'y' # for play # you can also use: p ; for power, or: f ; for food
test = Ttype + "\n" + text + "\n" + fk

try:

    # Send data
    print('sending {!r}'.format(test))
    sent = sock.sendto(test, server_address)

    # Receive response
    print('waiting to receive')
    data, server = sock.recvfrom(4096)
    print('received {!r}'.format(data))

finally:
    print('closing socket')
    sock.close()

# power event

# playing event

# feeding event

# status event