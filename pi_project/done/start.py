import os
import json
from sense_hat import SenseHat
from Animations import gender
import sunTest
from datetime import datetime

s = SenseHat()

# writen by Ziegler 10.12.19

gen = ""


def saveFile(gen):
    # the json obj
    data = {}

    sundata = sunTest.getSundata()
    date = datetime.now()

    # adding a 'status' array element to the json obj
    data['status'] = []
    # add one 'status' obj to the array
    data['status'].append({
        'mood': 10,
        'food': 10,
        'hp': 10,
        'gender': gen,
        'sunUp': sundata['sunRise'],
        'sunDown': sundata['sunSet'],
        'date': str(date.day) + "." + str(date.month) + "." + str(date.year)
        })

    with open('./savefile.txt', 'w') as output:
        json.dump(data, output)

    return data


def updateSunData():
    with open('./savefile.txt') as json_file:
        data = json_file[0]
        newKind = json.loads(data)[0]
        print(newKind['date'])


def startup():
    # check if first one
    if not os.path.isfile("./savefile.txt"):
        print("first time")
        s.show_message("left for girl, right for boy", 0.05, text_colour=[255, 0,0])
        s.set_pixels(gender()[0])

        event = s.stick.wait_for_event()
        print("The joystick was {} {}".format(event.action, event.direction))
        if event.direction == "left":
            gen = "girl"
        elif event.direction == "right":
            gen = "boy"

        print(gen)

        return saveFile(gen)
    # update savefile if the date is old
    # TODO check if the file have a old date, if then, call the API and update the save file
    else:

        with open('./savefile.txt') as json_file:
            data = json.load(json_file)
            for p in data['status']:
                print('food: ' + str(p['food']) )

        return data
