from sense_hat import SenseHat
import time

s = SenseHat()
s.low_light = True

green = (0, 255, 0)
yellow = (255, 255, 0)

red = (255, 0, 0)
white = (255,255,255)
nothing = (0,0,0)
blue = (0, 0, 255)
pink = (255,105, 180)

def getBackgroundColor(input):
    if input == "girl":
        return pink
    elif input == "boy":
        return blue



def getCurrentFace(input, foodColor, moodColor, hpColor, foodNum, moodNum, hpNum):
    # if mood is high, happy
    if moodNum >= 6:
        return happy2_pixels(input, foodColor, moodColor, hpColor)
    elif 3 < moodNum < 6:
        return idle(input, foodColor, moodColor, hpColor)
    elif moodNum <= 3:
        return mad_pixels(input, foodColor, moodColor, hpColor)
    else:
        return idle(input, foodColor, moodColor, hpColor)

def idle(input, food, mood, hp):
    F = food
    M = mood
    H = hp
    w = (255, 255, 255)
    g = (50, 205, 50)
    c = getBackgroundColor(input)
    photo1 = [
    M, c, c, c, c, c, c, H,
    c, c, c, c, c, c, c, c,
    c, c, g, c, c, g, c, c,
    c, c, c, c, c, c, c, c,
    c, c, c, c, c, c, c, c,
    c, c, g, g, g, g, c, c,
    c, c, c, c, c, c, c, c,
    F, c, c, c, c, c, c, c
    ]
    photos = [photo1]
    return photos

# grids made by Maher
def mad_pixels(input,food, mood, hp):
    F = food
    M = mood
    H = hp

    B = getBackgroundColor(input)
    R = red
    photo1 = [
    M, B, B, B, B, B, B, H,
    B, R, R, B, B, R, R, B,
    B, R, R, B, B, R, R, B,
    B, B, B, B, B, B, B, B,
    B, B, B, B, B, B, B, B,
    B, R, R, R, R, R, R, B,
    B, B, B, B, B, B, B, B,
    F, B, B, B, B, B, B, B,
    ]
    photo2 = [
    M, B, B, B, B, B, B, H,
    B, R, R, B, B, R, R, B,
    B, R, R, B, B, R, R, B,
    B, B, B, B, B, B, B, B,
    B, B, R, R, R, R, B, B,
    B, R, R, R, R, R, R, B,
    B, R, R, B, B, R, R, B,
    F, R, B, B, B, B, R, B,
    ]

    photos = [photo1, photo2]
    return photos

# grids made by Maher
def hunger1_pixels(input,food, mood, hp):
    F = food
    M = mood
    H = hp

    Y = yellow

    O = nothing
    B = getBackgroundColor(input)
    photo1 = [
    M, B, B, B, B, B, B, H,
    B, Y, Y, B, B, Y, Y, B,
    B, Y, Y, B, B, Y, Y, B,
    B, B, B, B, B, B, B, B,
    B, Y, Y, Y, Y, Y, Y, B,
    B, B, Y, O, O, Y, B, B,
    B, B, B, Y, Y, B, B, B,
    F, B, B, B, B, B, B, B,
    ]
    photo2 = [
    M, B, B, B, B, B, B, H,
    B, Y, Y, B, B, Y, Y, B,
    B, Y, Y, B, B, Y, Y, B,
    B, B, B, B, B, B, B, B,
    B, Y, Y, Y, Y, Y, Y, B,
    B, Y, O, O, O, O, Y, B,
    B, B, Y, O, O, Y, B, B,
    F, B, B, Y, Y, B, B, B,
    ]

    photos = [photo1, photo2]
    return photos

# grids made by Nicolai
def hunger2_pixels(input,food, mood, hp):
    F = food
    M = mood
    H = hp

    O = nothing
    B = getBackgroundColor(input)
    W = white

    photo1 = [
    M, B, B, B, B, B, B, H,
    B, W, W, B, B, W, W, B,
    B, W, W, B, B, W, W, B,
    B, B, B, B, B, B, B, B,
    B, W, W, W, W, W, W, B,
    B, B, W, O, O, W, B, B,
    B, B, B, W, W, B, B, B,
    F, B, B, B, B, B, B, B,

    ]
    photo2 = [
    M, B, B, B, B, B, B, H,
    B, W, W, B, B, W, W, B,
    B, W, W, B, B, W, W, B,
    B, B, B, B, B, B, B, B,
    B, W, W, W, W, W, W, B,
    B, W, O, O, O, O, W, B,
    B, B, W, O, O, W, B, B,
    F, B, B, W, W, B, B, B,
    ]

    photos = [photo1, photo2]
    return photos

# grids made by Nicolai
def bone_pixels(food, mood, hp):
    F = food
    M = mood
    H = hp
    O = nothing

    W = white

    photo1 = [
    M, O, O, O, O, O, O, H,
    O, O, O, O, O, W, O, O,
    O, O, O, O, O, W, W, O,
    O, O, O, O, W, O, O, O,
    O, O, O, W, O, O, O, O,
    O, W, W, O, O, O, O, O,
    O, O, W, O, O, O, O, O,
    F, O, O, O, O, O, O, O,
    ]
    photo2 = [
    M, O, O, O, O, O, O, H,
    O, O, W, O, O, O, O, O,
    O, W, W, O, O, O, O, O,
    O, O, O, W, O, O, O, O,
    O, O, O, O, W, O, O, O,
    O, O, O, O, O, W, W, O,
    O, O, O, O, O, W, O, O,
    F, O, O, O, O, O, O, O,

    ]

    photos = [photo1, photo2]
    return photos

# grids made by Nicolai
def ball_pixels(input,food, mood, hp):
    F = food
    M = mood
    H = hp
    O = nothing
    R = red
    lightred = (255, 90, 90)
    darkred = (155, 0, 0)
    LR = lightred
    DR = darkred
    photo1 = [
    M, O, O, O, O, O, O, H,
    O, O, O, R, R, O, O, O,
    O, O, R, R, LR, LR, O, O,
    O, DR, R, R, R, LR, R, O,
    O, DR, R, R, R, R, R, O,
    O, O, R, R, R, R, O, O,
    O, O, O, R, R, O, O, O,
    F, O, O, O, O, O, O, O,
    ]
    photos = [photo1]
    return photos

# grids made by Gitte
def happy2_pixels(input,food, mood, hp):
    F = food
    M = mood
    H = hp
    w = (255, 255, 255)
    g = (50, 205, 50)
    c = getBackgroundColor(input)
    photo1 = [
    M, c, c, c, c, c, c, H,
    c, g, g, c, c, g, g, c,
    c, g, g, c, c, g, g, c,
    c, c, c, c, c, c, c, c,
    c, g, c, c, c, c, g, c,
    c, c, g, g, g, g, c, c,
    c, c, c, c, c, c, c, c,
    F, c, c, c, c, c, c, c
    ]

    photos = [photo1]
    return photos

# grids made by Gitte
def happy_pixels(input, food, mood, hp):
    F = food
    M = mood
    H = hp
    w = (255, 255, 255)
    G = (50, 205, 50)
    B = getBackgroundColor(input)
    photo1 = [
    M, B, B, B, B, B, B, H,
    B, G, G, B, B, G, G, B,
    B, G, G, B, B, G, G, B,
    B, B, B, B, B, B, B, B,
    B, B, B, B, B, B, B, B,
    B, G, B, B, B, B, G, B,
    B, B, G, G, G, G, B, B,
    F, B, B, B, B, B, B, B,
    ]

    photo2 = [
    M, B, B, B, B, B, B, H,
    B, G, G, B, B, G, G, B,
    B, G, G, B, B, G, G, B,
    B, B, B, B, B, B, B, B,
    B, G, B, B, B, B, G, B,
    B, G, G, G, G, G, G, B,
    B, B, G, G, G, G, B, B,
    F, B, B, B, B, B, B, B,
    ]

    photos = [photo1, photo2]
    return photos

# grids made by Nicolai
def bouncingBall(food, mood, hp):
    F = food
    M = mood
    H = hp
    O = nothing
    R = red
    lightred = (255, 90, 90)
    darkred = (155, 0, 0)
    LR = lightred
    DR = darkred
    photo3 = [
    M, O, O, O, O, O, O, H,
    O, O, O, O, O, O, O, O,
    O, O, R, R, R, R, O, O,
    O, R, R, R, LR, LR, R, O,
    O, R, R, R, R, LR, R, O,
    O, R, R, R, R, R, R, O,
    O, R, R, R, R, R, R, O,
    F, O, R, R, R, R, O, O,
    ]
    photo1 = [
    M, O, R, R, R, R, O, H,
    O, R, R, R, LR, LR, R, O,
    O, R, R, R, R, LR, R, O,
    O, R, R, R, R, R, R, O,
    O, R, R, R, R, R, R, O,
    O, O, R, R, R, R, O, O,
    O, O, O, O, O, O, O, O,
    F, O, O, O, O, O, O, O,
    ]
    photo2 = [
    M, O, R, R, R, R, O, H,
    O, R, R, R, LR, LR, R, O,
    O, R, R, R, R, LR, R, O,
    O, R, R, R, R, R, R, O,
    O, R, R, R, R, R, R, O,
    O, O, R, R, R, R, O, O,
    O, O, O, O, O, O, O, O,
    F, O, O, O, O, O, O, O,
    ]
    photos = [photo1, photo2, photo3]
    return photos

# girl or boy
def gender():
    B = blue
    O = nothing
    p = pink

    photo1 = [
    O, O, p, O, O, B, O, O,
    O, O, p, O, O, B, O, O,
    O, p, p, O, O, B, B, O,
    p, p, p, O, O, B, B, B,
    p, p, p, O, O, B, B, B,
    O, p, p, O, O, B, B, O,
    O, O, p, O, O, B, O, O,
    O, O, p, O, O, B, O, O,
    ]
    photos = [photo1]
    return photos

# grids made by Ziegler
def sleep_pixels(input, food, mood, hp):
    F = food
    M = mood
    H = hp
    G = white
    B = getBackgroundColor(input)
    photo1 = [
    M, B, B, B, B, B, B, H,
    B, B, B, B, B, B, B, B,
    B, G, G, B, B, G, G, B,
    B, B, B, B, B, B, B, B,
    B, B, B, B, B, B, B, B,
    B, G, G, G, G, G, G, B,
    B, G, B, B, B, B, B, B,
    F, B, B, B, B, B, B, B,
    ]

    photo2 = [
    M, B, B, B, B, B, B, H,
    B, B, B, B, B, B, B, B,
    B, G, G, B, B, G, G, B,
    B, B, B, B, B, B, B, B,
    B, B, B, B, B, B, B, B,
    B, G, G, G, G, G, G, B,
    B, G, B, B, B, B, B, B,
    F, G, B, B, B, B, B, B,
    ]

    photos = [photo1, photo2]
    return photos
