import requests
import json

from datetime import datetime

URL = "https://soltider.dk/api"
headers = {
  "User-Agent": "Other",
  "Accept": "application/json",
  "Content-Type": "application/json"
}
p = {'lat':'55', 'lng':'12'}

def getSundata():
  r = requests.get(url = URL, params = p, headers = headers)

  now = datetime.now()

  #print(data)
  #print(data["sunRise"])
  #print(data["sunSet"])
  print(now)

  return json.loads(json.dumps(r.json()[0]))


def postFoodEvent(text, fkUserId):
  # food post
  newHead = {
    "Accept": "application/json",
    "Content-Type": "application/json"
}
  foodUrl = 'https://tami.azurewebsites.net/api/EventFood'
  foodObj = {'id': 0, 'text': text, 'when': datetime.now().strftime("%Y-%m-%dT%H:%M:%S"), 'fkUserId': fkUserId}
  x = requests.post(foodUrl, data=json.dumps(foodObj), headers = newHead)

  print(x)



def postPlayEvent(text, fkUserId):
  # play post
  newHead = {
    "Accept": "application/json",
    "Content-Type": "application/json"
  }
  playUrl = 'https://tami.azurewebsites.net/api/EventPlay'
  playObj = {'id': 0, 'text': text, 'when': datetime.now().strftime("%Y-%m-%dT%H:%M:%S"), 'fkUserId': fkUserId}
  x = requests.post(playUrl, data=json.dumps(playObj), headers = newHead)

  print(x)


def postStatusEvent(mood, food, hp, fkUserId):
  # status post
  newHead = {
    "Accept": "application/json",
    "Content-Type": "application/json"
  }
  statusUrl = 'https://tami.azurewebsites.net/api/Status'
  statusObj = {'id': 0, 'mood': mood, 'food': food, 'hp': hp, 'when': datetime.now().strftime("%Y-%m-%dT%H:%M:%S"), 'fkUserId': fkUserId}
  x = requests.post(statusUrl, data = json.dumps(statusObj), headers = newHead)

  print(x)


def postPowerEvent(text, fkUserId):
  # power post
  newHead = {
    "User-Agent": "Other",
    "Accept": "application/json",
    "Content-Type": "application/json"
  }
  powerUrl = 'https://tami.azurewebsites.net/api/EventPower'
  powerObj = {'id': 0, 'text': text, 'when': datetime.now().strftime("%Y-%m-%dT%H:%M:%S"), 'fkUserId': fkUserId}
  x = requests.post(powerUrl, data=json.dumps(powerObj), headers = newHead)

  print(x)


