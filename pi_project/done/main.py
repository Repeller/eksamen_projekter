import start
from sense_hat import SenseHat
import Animations
import saveHandler
from saveHandler import States
import datetime
import time
from threading import Thread
import sunTest

# reset the screen
s = SenseHat()
s.clear()
s.low_light = True


def setGlobal(stringKey, value):
    globals()[stringKey] = value


def getGlobal(key):
    return globals()[key]


setGlobal("shaken", False)

def switch(argument, input, food, mood, hp):
    if argument == States.sleep:
        return Animations.sleep_pixels(input, food, mood, hp)
    if argument == States.play:
        return Animations.bouncingBall(food, mood, hp)
    if argument == States.feed:
        return Animations.bone_pixels(food, mood, hp)
    if argument == States.display:
        return Animations.getCurrentFace(input, food, mood, hp, save.local_food, save.local_mood, save.local_hp)


def printing(local):
    print("hp=" + str(local.local_hp))
    print("mood=" + str(local.local_mood))
    print("sunup=" + str(local.local_sunUp))
    print("sundown=" + str(local.local_sunDown))
    print("food=" + str(local.local_food))
    print("date=" + str(local.local_date))
    print("gender=" + str(local.local_gender))
    print("current=",local.local_current)


def displayValues(number):
    if number >= 7:
        c = Animations.green
    elif 4 <= number < 7:
        c = Animations.yellow
    elif number == 2 or number == 3:
        c = Animations.red
    else:
        c = Animations.red

    return c


def tickAll():
    while True:
        if not save.local_current == States.sleep:
            oldFood = save.local_food
            oldMood = save.local_mood
            oldHp = save.local_hp
            save.tickMood()
            save.tickFood()
            save.tickHp()
            # add to database TODO: test if this works
            sunTest.postStatusEvent(save.local_mood, save.local_food, save.local_hp, 1)
            time.sleep(10)
            if not oldFood == save.local_food or not oldMood == save.local_mood or not oldHp == save.local_hp:
                save.saveToFile()


# TODO: all the values for the shaking


accel_thresh = 0.4



def checkShaking(sense):

    while True:
        x, y, z = sense.get_accelerometer_raw().values()
        x1 = abs(x)
        y1 = abs(y)
        z1 = abs(z)
        time.sleep(0.1)
        x, y, z = sense.get_accelerometer_raw().values()
        dx = abs(abs(x1) - abs(x))
        dy = abs(abs(y1) - abs(y))
        dz = abs(abs(z1) - abs(z))
        if dx > accel_thresh or dy > accel_thresh or dz > accel_thresh:
            print("true check have been made")
            setGlobal("shaken", True)
            print("shaken ", getGlobal("shaken"))

            print("the 'getJoy' shaken, is called")
            save.local_current = States.play
            # save play event TODO: test if this works
            sunTest.postPlayEvent('playing now', 1)

        time.sleep(1)


# first time start : pick gender (m/f)
savefile = start.startup()

#handler = saveHandler
save = saveHandler.loadFromFile()

printing(save)


# states: sleep, display, feed, play

def getJoy(sense):
    while True:
        print("Current: ", save.local_current)
        event = sense.stick.wait_for_event()

        if event.direction == "left":
            save.local_current = States.feed
            # save food event TODO: test if this works
            sunTest.postFoodEvent('feeding it now', 1)

        #TODO: remove this part, after you have showed it to the teacher

        elif event.action == "released" and event.direction == "up":
            if save.local_current == States.sleep:
                save.local_current = States.display

            elif event.action == "released" and save.local_current == States.display:
                save.local_current = States.sleep

            print("State force switched")

        time.sleep(1)


# TODO : clear the screen for the fist time
s.clear()

count = 0

sunOutTime = datetime.datetime.strptime(save.local_sunUp, '%H:%M').time()
sunDownTime = datetime.datetime.strptime(save.local_sunDown, '%H:%M').time()

Thread(target=tickAll).start()
Thread(target=getJoy, args=(s,)).start()
Thread(target=checkShaking, args=(s,)).start()



cycle = 0

while True:
    print("'testing' shaken: ", getGlobal("shaken"), "local_current: ", str(save.local_current))

    #cTime = datetime.datetime.now().time()
    cTime = datetime.datetime.strptime("14:30", "%H:%M").time()

    # if we have started an event
    if save.local_current == States.play:
        print("play event")
        if cycle == 6:
            save.local_current = States.display
            cycle = 0
            save.local_mood = 11
            save.saveToFile()
            #TODO: test if this works
            setGlobal("shaken", False)
            continue

        cycle += 1

    elif save.local_current == States.feed:
        print("feed event")
        if cycle == 6:
            save.local_current = States.display
            cycle = 0
            save.local_food = 11
            save.saveToFile()
            continue

        cycle += 1


    # set face animation and the status light
    ani = switch(save.local_current, save.local_gender, displayValues(save.local_food),
                 displayValues(save.local_mood), displayValues(save.local_hp))

    # run animation on the screen
    countOfPhotos = len(ani)
    if count > countOfPhotos:
        count = 0
    elif count != countOfPhotos:
        s.set_pixels(ani[count])
    elif count == countOfPhotos:
        count = -1

    time.sleep(.3)
    count += 1

