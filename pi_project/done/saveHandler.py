import threading
import enum
import json
import os

# props
class types(enum.Enum):
    mood = 1
    food = 2
    health = 3


class States(enum.Enum):
    sleep = 1
    display = 2
    feed = 3
    play = 4


# props


class local(object):
    local_mood = None
    local_hp = None
    local_food = None
    local_gender = None
    local_sunUp = None
    local_sunDown = None
    local_date = None
    local_current = None

    def __init__(self, mood, hp, food, gender, sunUp, sunDown, date, current):
        self.local_mood = mood
        self.local_hp = hp
        self.local_food = food
        self.local_gender = gender
        self.local_sunUp = sunUp
        self.local_sunDown = sunDown
        self.local_date = date
        self.local_current = current


    def tickHp(self):
        print("Ticking hp")
        if self.local_food > 5 and self.local_mood > 5:
            if self.local_hp < 10:
                self.local_hp += 1
        elif self.local_food < 4 and self.local_mood < 4:
            if self.local_hp > 1:
                self.local_hp -= 1
        print(self.local_hp)

    def tickFood(self):
        print("Ticking Food")
        if self.local_food > 1:
            self.local_food -= 1
            print(str(self.local_food))

    def tickMood(self):
        print("Ticking Mood")
        if self.local_mood > 1:
            self.local_mood -= 1
            print(str(self.local_mood))

    def saveToFile(self):
        data = {}
        data['status'] = []
        # add one 'status' obj to the array
        data['status'].append({
            'mood': self.local_mood,
            'food': self.local_food,
            'hp': self.local_hp,
            'gender': self.local_gender,
            'sunUp': self.local_sunUp,
            'sunDown': self.local_sunDown,
            'date': self.local_date
        })
        with open('./savefile.txt', 'w') as output:
            json.dump(data, output)



"" # this is a time code xx:xx 24hours

def getSavedata():
    # the json obj
    data = {}

    with open('./savefile.txt') as json_file:
        data = json.load(json_file)
    return data

#returns a obj class of type local
def loadFromFile():
    with open('./savefile.txt', "r") as json_file:
        testdata = json.loads(json_file.read())

        x = None
        for p in testdata['status']:
            x = local(p['mood'], p['hp'], p['food'], p['gender'], p['sunUp'], p['sunDown'], p['date'], States.display)

        return x


