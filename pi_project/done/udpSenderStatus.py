import saveHandler
import json
import socket
import sys

# Create a UDP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

server_address = ('https://localhost:44326/', 11678)
message = b'This is the message.  It will be repeated.'

mood = 1
food = 1
hp = 1
fk = 1
Ttype = 's' # s for status
test = (str(Ttype) + '\n' + str(mood) + '\n' + str(food) + '\n' + str(hp) + '\n' + str(fk)).encode()


try:

    # Send data
    print('sending {!r}'.format(test))
    sent = sock.sendto(test, server_address)

    # Receive response
    #print('waiting to receive')
    #data, server = sock.recvfrom(4096)

    #print('received {!r}'.format(data))

finally:
    print('closing socket')
    sock.close()
